# aws_iot_certificate cert
resource "aws_iot_certificate" "cert" {
  active = true
}


# aws_iot_policy pub-sub
resource "aws_iot_policy" "pub_sub" {
  name   = "pub_sub_policy"
  policy = file("${path.module}/files/iot_policy.json")
}


# aws_iot_policy_attachment attachment
resource "aws_iot_policy_attachment" "attachment" {
  policy = aws_iot_policy.pub_sub.name
  target = aws_iot_certificate.cert.arn
}


# aws_iot_thing temp_sensor
resource "aws_iot_thing" "temp_sensor" {
  name = "temperature_sensor_1"
}


# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "thing_attachment" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.temp_sensor.name
}


# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "endpoint" {
  endpoint_type = "iot:Data-ATS"
}



# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "invalid_data_to_dynamodb" {
  name        = "invalid_data_to_dynamodb"
  description = "Route invalid data to DynamoDB"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+' WHERE temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn    = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.temperature.name
    }
  }
}




# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "valid_data_to_timestream" {
  name        = "valid_data_to_timestream"
  description = "Route valid data to Timestream"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream {
    database_name = aws_timestreamwrite_database.iot_database.database_name
    table_name    = aws_timestreamwrite_table.temperature_sensor_table.table_name
    role_arn      = aws_iam_role.iot_role.arn

    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }
    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }
    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }
    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }
  }
}



###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
