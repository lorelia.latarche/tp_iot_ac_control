# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "iot_database" {
  database_name = "iot"
}


# aws_timestreamwrite_table linked to the database
resource "aws_timestreamwrite_table" "temperature_sensor_table" {
  database_name = aws_timestreamwrite_database.iot_database.database_name
  table_name    = "temperaturesensor"
  retention_properties {
    memory_store_retention_period_in_hours = 24
    magnetic_store_retention_period_in_days = 7
  }
}

  